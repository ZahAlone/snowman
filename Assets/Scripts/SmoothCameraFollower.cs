﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class SmoothCameraFollower : MonoBehaviour
{
    [SerializeField] Transform Target;
    [SerializeField] float FollowSpeed = 1f;
    [SerializeField] float YOffset = 0f;
    Camera Camera;    

    private void Awake()
    {
        Camera = GetComponent<Camera>();
        if (Target == null) Target = FindObjectOfType<PlayerController>().transform;
    }

    void Update()
    {
        if (Target != null)
        {
            Vector3 newPos = new Vector3(
                Target.position.x, 
                Target.position.y + YOffset, 
                transform.position.z);

            transform.position = Vector3.Lerp(transform.position, newPos, 
                FollowSpeed * Time.deltaTime);
        }
    }
}
