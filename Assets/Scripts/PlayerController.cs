﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D), typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour
{
    public float Velocity = 10f;
    public float JumpForce = 10f;

    public Transform GroundCheckTransform;
    public float GroundCheckSize = 1f;
    public LayerMask GroundLayers;

    public AnimatorNameHash SpeedParamHash;
    public AnimatorNameHash VertSpeedParamHash;
    public AnimatorNameHash JumpParamHash;
    public AnimatorNameHash JumpStartStateHash;
    public AnimatorNameHash JumpBlendTreeHash;
    public AnimatorNameHash AttackParamHash;

    enum State { Grounded, JumpStart, InAir, Shoot, ShootEnd };
    State CurrentState = State.Grounded;
    Rigidbody2D Rigidbody;
    Animator Animator;
    SpriteRenderer SpriteRenderer;


    private void Awake()
    {
        Rigidbody = GetComponent<Rigidbody2D>();
        Animator = GetComponent<Animator>();
        SpriteRenderer = GetComponent<SpriteRenderer>();
        SpeedParamHash.Init();
        VertSpeedParamHash.Init();
        JumpParamHash.Init();
        JumpStartStateHash.Init();
        JumpBlendTreeHash.Init();
        AttackParamHash.Init();
    }

    private void FixedUpdate()
    {
        switch(CurrentState)
        {
            case State.Grounded:

                if (!CheckGround)
                {
                    CurrentState = State.InAir;
                    Animator.CrossFade(JumpBlendTreeHash.Hash, 0.1f);
                }
                else if (Input.GetAxis("Jump") > 0f)
                {
                    CurrentState = State.JumpStart;
                    Animator.CrossFade(JumpStartStateHash.Hash, 0.1f);
                    Invoke("AddJumpForce", 0.12f);
                }
                else if (Input.GetAxis("Shoot") > 0f)
                {
                    CurrentState = State.Shoot;
                    Animator.SetBool(AttackParamHash.Hash, true);
                }                

                break;


            case State.JumpStart:

                if(!CheckGround) CurrentState = State.InAir;

                break;
                
            case State.InAir:

                if (CheckGround) CurrentState = State.Grounded;

                break;
        }

        float xVel = Input.GetAxis("Horizontal");
        Rigidbody.velocity = new Vector2(Velocity * xVel, Rigidbody.velocity.y);
        Animator.SetFloat(SpeedParamHash.Hash, Mathf.Abs(Rigidbody.velocity.x));
        Animator.SetFloat(VertSpeedParamHash.Hash, Rigidbody.velocity.y);
        
        if ((xVel > 0f && SpriteRenderer.flipX) || (xVel < 0f && !SpriteRenderer.flipX))
        {
            SpriteRenderer.flipX = !SpriteRenderer.flipX;
        }
    }

    public void SetState(int state)
    {
        CurrentState = (State)state;
    }

    void SetInAirState()
    {
        CurrentState = State.InAir;
    }

    public void AddJumpForce()
    {
        if(CurrentState == State.JumpStart)
        {
            Rigidbody.AddForce(new Vector2(0f, JumpForce), ForceMode2D.Impulse);
            Invoke("SetInAirState", 0.1f);
        }
    }

    public void AnimShoot()
    {

    }

    public void EndShoot()
    {

    }
    
    bool CheckGround
    {
        get
        {
            bool isOverlap = Physics2D.OverlapBox(GroundCheckTransform.position,
                new Vector2(GroundCheckSize, GroundCheckSize), 0f, GroundLayers) != null;

            Animator.SetBool(JumpParamHash.Hash, !isOverlap);
            return isOverlap;
        }
    }

    private void OnDrawGizmos()
    {
        if(GroundCheckTransform != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(GroundCheckTransform.position, 
                new Vector3(GroundCheckSize, GroundCheckSize, GroundCheckSize));
        }
    }
}

[System.Serializable]
public class AnimatorNameHash
{
    public string Name;
    public int Hash { get; private set; }
    
    public void Init()
    {
        Hash = Animator.StringToHash(Name);
    }
}